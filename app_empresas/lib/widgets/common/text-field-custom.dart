import 'package:flutter/material.dart';

class TextFieldCustom extends StatelessWidget {
  TextFieldCustom(this.textEditingcontroller,
      {this.hintText = '',
      this.onChanged,
      this.labelText = '',
      this.obscureText = false,
      this.iconPrefix,
      this.keyboardType = TextInputType.text
      });

  final TextEditingController textEditingcontroller;
  final Function(String)? onChanged;
  final bool obscureText;
  final String hintText;
  final String labelText;
  final Icon? iconPrefix;
  final TextInputType keyboardType;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 8),
      decoration: BoxDecoration(
        color: Colors.grey[200],
        borderRadius: BorderRadius.circular(5)
      ),
      child: TextField(
        onChanged: this.onChanged,
        controller: this.textEditingcontroller,
        obscureText: this.obscureText,
        keyboardType: this.keyboardType,
        decoration: InputDecoration(
            prefixIcon: iconPrefix,
            hintText: this.hintText,
            labelText: this.labelText,
            border: InputBorder.none),
      ),
    );
  }
}
