import 'package:app_empresas/models/enterprise.dart';
import 'package:app_empresas/pages/enterprises/enterprise-page.dart';
import 'package:flutter/material.dart';

class EnterpriseTile extends StatelessWidget {
  EnterpriseTile(this.enterprise);

  final Enterprise enterprise;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => goToEnterprisePage(context),
      child: Container(
        padding: EdgeInsets.only(bottom: 34),
        height: 120,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4),
          image: DecorationImage(
            fit: BoxFit.cover,
            image: Image.network(enterprise.photo).image,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Text(
              enterprise.enterpriseName,
              style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 20,
                  color: Colors.white),
            ),
          ],
        ),
      ),
    );
  }

  void goToEnterprisePage(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => EnterprisePage(this.enterprise),
      ),
    );
  }
}
