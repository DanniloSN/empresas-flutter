import 'package:flutter/material.dart';

// ignore: must_be_immutable
class LoadingDialog extends StatelessWidget {
  LoadingDialog(BuildContext context, Future<dynamic> function, {successCallback, errorCallback}) {
    function.then((response) {
      _isLoading = false;
      Navigator.pop(context);
      if (successCallback != null) successCallback(response);
    }).catchError((error) {
      _isLoading = false;
      Navigator.pop(context);
      if (errorCallback != null) errorCallback(error);
    });
  }

  void printResponse(response) => print(response);

  bool _isLoading = true;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future.value(!_isLoading),
      child: Dialog(
        backgroundColor: Colors.transparent,
        elevation: 0,
        child: Center(
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation(Colors.white),
          ),
        ),
      ),
    );
  }
}
