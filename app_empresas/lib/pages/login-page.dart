import 'package:app_empresas/blocs/user-bloc.dart';
import 'package:app_empresas/widgets/common/text-field-custom.dart';
import 'package:app_empresas/widgets/dialogs/loading-dialog.dart';
import 'package:flutter/material.dart';
import 'package:app_empresas/blocs/auth-bloc.dart';
import 'package:app_empresas/pages/enterprises/enterprises-page.dart';

class LoginPage extends StatelessWidget {
  final _emailController =
      TextEditingController(text: 'testeapple@ioasys.com.br');
  final _passwordController = TextEditingController(text: '12341234');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            expandedHeight: 200,
            centerTitle: true,
            title: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image(image: Image.asset('assets/images/symbol.png').image),
                Text('Seja bem vindo ao empresas!')
              ],
            ),
            flexibleSpace: FlexibleSpaceBar(
              background:
                  Image.asset('assets/images/background.png', fit: BoxFit.fill),
            ),
          ),
          SliverToBoxAdapter(
            child: SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  SizedBox(height: 10),
                  TextFieldCustom(_emailController, labelText: 'E-mail'),
                  SizedBox(height: 10),
                  TextFieldCustom(
                    _passwordController,
                    labelText: 'Senha',
                    obscureText: true,
                  ),
                  SizedBox(height: 16),
                  ElevatedButton(
                    child: Text('ENTRAR'),
                    onPressed: () => login(
                      context,
                      _emailController.text,
                      _passwordController.text,
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  void login(BuildContext context, String email, String password) {
    showDialog(
        context: context,
        builder: (context) => LoadingDialog(
            context, AuthBloc.login(email, password),
            successCallback: (user) {
              UserBloc.user = user;
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => EnterprisesPage()));
            },
            errorCallback: (error) => ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text(error.toString())))));
  }
}
