import 'package:app_empresas/pages/login-page.dart';
import 'package:flutter/material.dart';

class SplashscreenPage extends StatelessWidget {
  void enterApp(BuildContext context, Widget page) {
    Future.delayed(Duration(seconds: 2)).then((value) =>
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => page)));
  }

  @override
  Widget build(BuildContext context) {
    enterApp(context, LoginPage());
    return Scaffold(
      body: Container(
        child: Image(image: Image.asset('assets/images/logo.png').image),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.cover,
            image: Image.asset('assets/images/background.png').image,
          ),
        ),
      ),
    );
  }
}
