import 'package:app_empresas/models/enterprise.dart';
import 'package:flutter/material.dart';

class EnterprisePage extends StatelessWidget {
  EnterprisePage(this.enterprise);

  Enterprise enterprise;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          leading: IconButton(
            onPressed: () => Navigator.pop(context),
            icon: Icon(Icons.arrow_back, color: Theme.of(context).primaryColor),
          ),
          title: Text(
            enterprise.enterpriseName,
            style: TextStyle(color: Colors.black),
          ),
          centerTitle: true),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Image(image: Image.network(enterprise.photo).image),
            Container(
              padding: EdgeInsets.all(16),
              child: Text(
                enterprise.description,
                textAlign: TextAlign.justify,
              ),
            )
          ],
        ),
      ),
    );
  }
}
