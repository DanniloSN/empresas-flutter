import 'package:app_empresas/models/enterprise.dart';
import 'package:app_empresas/widgets/common/text-field-custom.dart';
import 'package:flutter/material.dart';
import 'package:app_empresas/blocs/user-bloc.dart';
import 'package:app_empresas/blocs/enterprise-bloc.dart';
import 'package:app_empresas/widgets/tiles/enterprise-tile.dart';

class EnterprisesPage extends StatefulWidget {
  @override
  _EnterprisesPageState createState() => _EnterprisesPageState();
}

class _EnterprisesPageState extends State<EnterprisesPage> {
  final _enterpriseController = TextEditingController();
  final _enterpriseBloc = new EnterpriseBloc(UserBloc.user);

  List _enterprises = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            pinned: true,
            expandedHeight: 200,
            title: TextFieldCustom(
              _enterpriseController,
              onChanged: searchEnterprise,
              hintText: 'Buscar por empresas (min 3 caracteres)',
              iconPrefix: Icon(Icons.search),
            ),
            flexibleSpace: FlexibleSpaceBar(
              background: Image.asset(
                'assets/images/background.png',
                fit: BoxFit.fill,
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
                padding: EdgeInsets.all(16),
                child: Text(_enterprises.length > 0
                    ? '${_enterprises.length} ${_enterprises.length > 1 ? 'resultados encontrados' : 'resultado encontrado'}'
                    : 'Nenhum resultado encontrado')),
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              List<Widget>.from(
                _enterprises.map((enterpriseData) {
                  final enterprise = new Enterprise.fromJson(enterpriseData);
                  return Padding(
                      padding: EdgeInsets.fromLTRB(16, 0, 16, 8),
                      child: EnterpriseTile(enterprise));
                }),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void searchEnterprise(String name) async {
    if (name.length < 3) return;
    try {
      final List enterprises =
          await this._enterpriseBloc.searchEnterprises(name);
      setState(() => this._enterprises = enterprises);
      print(enterprises);
    } catch (e) {
      print(e);
    }
  }
}
