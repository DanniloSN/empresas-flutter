class EnterpriseType {
  late int id;
  late String enterpriseTypeName;

  EnterpriseType.fromJson(Map<String, dynamic> json)
  : id = json['id'] ?? 0,
    enterpriseTypeName = json['enterprise_type_name'] ?? '';

  Map<String, dynamic> toJson() => {
    'id': id,
    'enterpriseTypeName': enterpriseTypeName
  };

}