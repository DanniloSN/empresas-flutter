import 'package:app_empresas/models/portfolio.dart';

class User {
  final int id;
  final String investorName;
  final String email;
  final String city;
  final String country;
  final double balance;
  final String photo;
  final Portifolio portfolio;
  final double portfolioValue;
  final bool firstAccess;
  final bool superAngel;

  late String accessToken;
  late String client;
  late String uid;

  void setAccessToken(String accessToken) => this.accessToken = accessToken;
  void setClient(String client) => this.client = client;
  void setUid(String uid) => this.uid = uid;

  User.fromJson(Map<String, dynamic> json)
      : id = json['id'] ?? 0,
        investorName = json['investor_name'] ?? '',
        email = json['email'] ?? '',
        city = json['city'] ?? '',
        country = json['country'] ?? '',
        balance = json['balance'] ?? 0,
        photo = json['photo'] != null ? 'https://empresas.ioasys.com.br${json['photo']}' : '',
        portfolio = new Portifolio.fromJSON(
          json['portfolio'] ?? {'enterprises_number': 0, 'enterprises': []},
        ),
        portfolioValue = json['portfolio_value'] ?? 0,
        firstAccess = json['first_access'] ?? false,
        superAngel = json['super_angel'] ?? false;

  Map<String, dynamic> toJson() => {
        'id': id,
        'investorName': investorName,
        'email': email,
        'city': city,
        'country': country,
        'balance': balance,
        'photo': photo,
        'portfolio': portfolio.toJson(),
        'portfolioValue': portfolioValue,
        'firstAccess': firstAccess,
        'superAngel': superAngel,
        'accessToken': accessToken,
        'client': client,
        'uid': uid
      };
}
