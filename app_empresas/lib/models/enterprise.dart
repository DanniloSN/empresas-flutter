import 'package:app_empresas/models/enterprise-type.dart';

class Enterprise {
  final int id;
  final String emailEnterprise;
  final String facebook;
  final String twitter;
  final String linkedin;
  final String phone;
  final bool ownEnterprise;
  final String enterpriseName;
  final String photo;
  final String description;
  final String city;
  final String country;
  final int value;
  final double sharePrice;
  final EnterpriseType enterpriseType;

  Enterprise.fromJson(Map<String, dynamic> json)
      : id = json['id'] ?? 0,
        emailEnterprise = json['email_enterprise'] ?? '',
        facebook = json['facebook'] ?? '',
        twitter = json['twitter'] ?? '',
        linkedin = json['linkedin'] ?? '',
        phone = json['phone'] ?? '',
        ownEnterprise = json['own_enterprise'] ?? false,
        enterpriseName = json['enterprise_name'] ?? '',
        photo = json['photo'] != null ? 'https://empresas.ioasys.com.br${json['photo']}' : '',
        description = json['description'] ?? '',
        city = json['city'] ?? '',
        country = json['country'] ?? '',
        value = json['value'] ?? 0,
        sharePrice = json['share_price'] ?? '',
        enterpriseType = new EnterpriseType.fromJson(
            json['enterprise_type'] ?? {'id': 0, 'enterprise_type_name': ''});

  Map<String, dynamic> toJson() => {
        'id': id,
        'emailEnterprise': emailEnterprise,
        'facebook': facebook,
        'twitter': twitter,
        'linkedin': linkedin,
        'phone': phone,
        'ownEnterprise': ownEnterprise,
        'enterpriseName': enterpriseName,
        'photo': photo,
        'description': description,
        'city': city,
        'country': country,
        'value': value,
        'sharePrice': sharePrice,
        'enterpriseType': enterpriseType.toJson()
      };
}
