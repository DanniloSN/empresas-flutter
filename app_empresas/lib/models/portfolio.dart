import 'package:app_empresas/models/enterprise.dart';

class Portifolio {
  late int enterprisesNumber;
  late List<Enterprise> enterprises;

  Portifolio.fromJSON(Map<String, dynamic> json)
      : enterprisesNumber = json['enterprises_number'] ?? 0,
        enterprises = json['enterprises'] != null
            ? List<Enterprise>.from(json['enterprises']
                .map((enterprise) => new Enterprise.fromJson(enterprise)))
            : [];

  Map<String, dynamic> toJson() =>
      {'enterprisesNumber': enterprisesNumber, 'enterprises': enterprises};

}
