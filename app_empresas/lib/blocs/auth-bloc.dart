import 'dart:convert';
import 'package:http/http.dart' as Http;
import 'package:app_empresas/models/user.dart';

class AuthBloc {
  static Future<User> login(String email, String password) async {
    if (email.isEmpty || password.isEmpty) throw 'E-mail e senha obrigatórios!';

    final url =
        Uri.parse('https://empresas.ioasys.com.br/api/v1/users/auth/sign_in');
    final headers = {'Content-Type': 'application/json'};
    final body =
        jsonEncode({'email': email.toLowerCase().trim(), 'password': password});

    final response = await Http.post(url, body: body, headers: headers);
    if (response.statusCode != 200) {
      print('Login Error: ${response.body}');
      throw 'E-mail ou senha incorretos!';
    }

    final responseHeaders = response.headers;
    final responseBody = jsonDecode(response.body);
    if (responseBody['investor'] == null) throw 'Resposta inválida da autenticação';

    final user = new User.fromJson(responseBody['investor']);
    user.setAccessToken(responseHeaders['access-token'] ?? '');
    user.setClient(responseHeaders['client'] ?? '');
    user.setUid(responseHeaders['uid'] ?? '');

    return user;
  }
}
