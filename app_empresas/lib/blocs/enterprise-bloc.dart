import 'dart:convert';
import 'package:app_empresas/models/user.dart';
import 'package:http/http.dart' as Http;

class EnterpriseBloc {
  EnterpriseBloc(this.user);

  late User user;

  Future<List> searchEnterprises(String name) async {
    final url = Uri.parse(
        "https://empresas.ioasys.com.br/api/v1/enterprises?name=$name");
    final headers = {
      'access-token': this.user.accessToken,
      'client': this.user.client,
      'uid': this.user.uid
    };

    final response = await Http.get(url, headers: headers);
    if (response.statusCode != 200) {
      print('Error seaching enterprises: ${response.body}');
      throw 'Não foi possível carregar empresas';
    }

    final responseBody = jsonDecode(response.body);
    return responseBody['enterprises'];
  }

  Future<dynamic> getEnterprise(int id) async {
    final url = Uri.parse("https://empresas.ioasys.com.br/api/v1/enterprises/$id");
    final headers = {
      'access-token': this.user.accessToken,
      'client': this.user.client,
      'uid': this.user.uid
    };

    final response = await Http.get(url, headers: headers);
    if (response.statusCode != 200) {
      print('Error getting enterprise with Id: ${response.body}');
      throw 'Não foi possível carregar dados da empresa';
    }

    return jsonDecode(response.body);
  }
}
