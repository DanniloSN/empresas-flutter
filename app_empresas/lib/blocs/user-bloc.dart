import 'package:app_empresas/models/user.dart';

class UserBloc {
  static final UserBloc _userBloc = UserBloc._internal();
  factory UserBloc() => _userBloc;
  UserBloc._internal();

  static late User user;
}