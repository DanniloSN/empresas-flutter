import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:app_empresas/pages/splashscreen-page.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: Colors.transparent));
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Empresas App',
      theme: ThemeData(primarySwatch: Colors.pink),
      home: SplashscreenPage(),
      debugShowCheckedModeBanner: false,
    );
  }
}
